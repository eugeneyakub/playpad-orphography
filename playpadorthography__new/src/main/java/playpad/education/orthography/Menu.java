package playpad.education.orthography;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created with IntelliJ IDEA.
 * User: oleg
 * Date: 07.01.13
 * Time: 15:09
 * To change this template use File | Settings | File Templates.
 */
public class Menu extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.menu);

        package_name = getApplicationContext().getPackageName();
        bt_go = (Button) findViewById(R.id.button_go);                        //    игровая активити
        bt_link = (Button) findViewById(R.id.button_link);                    //    переход на сайт
        bt_about = (Button) findViewById(R.id.button_about);                  //    о программе

        bt_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                int resourceId = res.getIdentifier("click", "raw", package_name);
                playFromResource(resourceId);
                Intent i = new Intent(Menu.this, Go.class);
                startActivity(i);
            }
        });

        bt_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                int resourceId = res.getIdentifier("click", "raw", package_name);
                playFromResource(resourceId);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.playpads.net"));
                startActivity(browserIntent);
            }
        });

        bt_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                int resourceId = res.getIdentifier("click", "raw", package_name);
                playFromResource(resourceId);

                AlertDialog ad;
                AlertDialog.Builder builder = new AlertDialog.Builder(Menu.this);
                builder.setIcon(R.drawable.icon)
                        .setTitle(R.string.about_header)
                        .setMessage(R.string.about_text)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }

                        });
                ad = builder.create();
                ad.show();
            }
        });

        res = getResources();
    }


    public static MediaPlayer _mediaPlayer;
    public static Resources res;
    public static String package_name;

    public  void playFromResource(int resId)
    {
        if (_mediaPlayer != null)
        {
            _mediaPlayer.reset();
        }
        _mediaPlayer = MediaPlayer.create(this, resId);
        _mediaPlayer.start();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public static Button bt_go, bt_about, bt_link;


}