package playpad.education.orthography;

import android.R;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: oleg
 * Date: 06.01.13
 * Time: 17:04
 * To change this template use File | Settings | File Templates.
 */
//  активити с выбором буквы
public class LetterSelectActivity extends ListActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Использование собственного шаблона

        global_init();                                    //    получаем все буквы
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.simple_list_item_1,  letters);
        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        String item = (String) getListAdapter().getItem(position);
       // Toast.makeText(this, item + " selected", Toast.LENGTH_LONG).show();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("item",item);
        returnIntent.putExtra("position",position);
        setResult(RESULT_OK,returnIntent);
        finish();

    }

    String locale_current;
    int letter_count, letter_type;
    String[] letters;


    public void global_init(){
        locale_current = Locale.getDefault().getLanguage();
        if (locale_current.toLowerCase().contains("ru"))
        {
            letter_count = 26;
            letter_type = 0;
            letters = new String[]{"A", "a", "Б", "б","В", "в", "Г", "г","Д", "д", "Е", "е", "Ё", "ё","Ж", "ж", "З", "з","И", "и",
                    "Й", "й", "К", "к","Л", "л", "М", "м","Н", "н", "О", "о", "П", "п","Р", "р", "С", "с","Т", "т",
                    "У", "у", "Ф", "ф","Х", "х", "Ц", "ц","Ч", "ч", "Ш", "ш","Щ","щ","ъ","ы", "ь", "Э", "э", "Ю", "ю", "Я", "я"};
        }
        else if (locale_current.toLowerCase().contains("en"))
        {
            letter_count = 33;
            letter_type = 0;
            letters = new String[]{"A", "a", "B", "b","C", "c", "D", "d","E", "e", "F", "f", "G", "g","H", "h", "I", "i","J", "j",
                                        "K", "k", "L", "l","M", "m", "N", "n","O", "o", "P", "p", "Q", "q","R", "r", "S", "s","T", "t",
                                        "U", "u", "V", "v","W", "w", "X", "x","Y", "y", "Z", "z"};
        }
        else if (locale_current.toLowerCase().contains("zh"))
        {
            letter_count = 400;
            letter_type = 1;

            letters = new String[]{"一", "二", "三", "人","土", "山", "口", "大","小", "上", "下", "日","月", "木", "天", "石",
                    "田", "气","火", "水", "地", "云","风", "雨", "四", "五","六", "七", "八","九", "十",
                    "多", "少","女", "男", "他", "她","不", "好", "你", "我","妈", "爸", "爷","奶", "哥",
                    "弟", "姐"};//,"妹", "家"  };

        };
    }
}