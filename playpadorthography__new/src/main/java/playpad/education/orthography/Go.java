package playpad.education.orthography;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.*;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.Html;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;

import java.util.Locale;

public  class Go extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public static MediaPlayer _mediaPlayer;

    public  void playFromResource(int resId)
    {
        if (_mediaPlayer != null)
        {
            _mediaPlayer.reset();
        }
        _mediaPlayer = MediaPlayer.create(this, resId);
        _mediaPlayer.start();
    }

    public static Button b_eraser, b_check, b_sound, b_letters;
    public Draw2D dd;
    public Context context;
    public static ProgressBar  error_progress;
    public static ImageView iv_smile;
    public static TextView tv_error_count;
    public static Resources res;
    public static int initial_left_margin = 100;
    public static int initial_right_margin = 100;
    public static int initial_top_margin = 100;
    public static int initial_bottom_margin = 100;

    public static Vibrator vozdaytel;

    public  void  play_letter(){
        int a = Go.letter_current;
        if (Go.letter_current == 0)
            a = 1;

        if ( locale_current.toLowerCase().contains("en"))     {
            if (a % 2 == 0)
                a = a / 2;
            else a = a / 2 + 1;
        }


        if (locale_current.toLowerCase().contains("ru") )     {
            if (a < 56) {
                if (a % 2 == 0)
                    a = a / 2;
                else a = a / 2 + 1;
            } else if (a == 56){
                a = 29;
            } else if (a == 57){
                a = 30;
            } else if (a == 58 || a  == 59){
                a = 31;
            } else if (a == 60 || a  == 61){
                a = 32;
            } else if (a == 62 || a  == 63){
                a = 33;
            }
        }

        String res_name = "char_"+ Integer.toString(a);

        int resourceId2 = res.getIdentifier(res_name, "raw", package_name);

        playFromResource(resourceId2);
    }

    // перемещение числа ошибок по экрану
    public static void move_textview(){
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)tv_error_count.getLayoutParams();
        if (params.leftMargin + 10 <= 410) {
            params.setMargins(params.leftMargin + 10, params.topMargin, params.rightMargin, params.bottomMargin); //substitute parameters for left, top, right, bottom
            tv_error_count.setLayoutParams(params);
        }
    }
    // восстановление положения числа ошибок
    public static void restore_tv_margin(){
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)tv_error_count.getLayoutParams();
        tv_error_count.setText(Html.fromHtml("<font color=red>&nbsp;" + "0"+ "</font>" + "&nbsp;/" + Integer.toString(Draw2D.num_max_error)));


        params.setMargins(initial_left_margin, initial_top_margin, initial_right_margin, initial_bottom_margin); //substitute parameters for left, top, right, bottom
        tv_error_count.setLayoutParams(params);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        package_name = getApplicationContext().getPackageName();

        this.global_init();

        setContentView(R.layout.main);



        context = getApplicationContext();
        // нижние кнопки: выбор буквы, ластик, проверка написания, звук
        b_check = (Button)findViewById(R.id.button_check);
        b_eraser = (Button)findViewById(R.id.button_eraser);
        b_letters = (Button)findViewById(R.id.button_letter_choose);
        b_sound = (Button)findViewById(R.id.button_sound);

        // вьюха для рисования
        dd = (Draw2D) findViewById(R.id.custom_canvas);

        b_eraser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                // проигрываем звук кликанья по кнопке
                int resourceId = res.getIdentifier("click", "raw", package_name);
                playFromResource(resourceId);

                // очищаем вьюху
                Draw2D.clear_canvas();
                dd.invalidate();    // применяем изменения на экран
                restore_tv_margin();// восставливаем положение окошка с ошибками


            }
        });

        b_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b_check.setEnabled(false);
                b_check.setClickable(false);
                /*
                Toast.makeText(context,
                        "i_point " + Integer.toString(dd.i_point), Toast.LENGTH_SHORT)
                        .show();   */
                // число точек меньше требуемого для написания
                if (dd.i_point < dd.num_min_achieved
                        ) {
                    int resourceId = res.getIdentifier("click", "raw", package_name);
                    playFromResource(resourceId);

                    Toast.makeText(context,
                            res.getString(R.string.fill_letter), Toast.LENGTH_SHORT)
                            .show();
                    b_check.setEnabled(true);
                    b_check.setClickable(true);
                } else {   // число точек достигло требуемого и приемлемый процент ошибок
                    if (dd.i_error < dd.num_max_error * dd.err_percent) {

                       ;
                        Toast.makeText(context,
                                res.getString(R.string.well_done), Toast.LENGTH_SHORT)
                                .show();
                       if (locale_current.toLowerCase().contains("ru") ||  locale_current.toLowerCase().contains("en"))  {
                           play_letter();
                           iv_smile.setImageResource(R.drawable.happy);
                           new Handler().postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   dd.change_letter(1);
                                   iv_smile.setImageResource(R.drawable.normal);
                                   error_progress.setProgress(0) ;
                                   restore_tv_margin();

                                   dd.invalidate();
                                   error_progress.setProgress(0);  //  прогресс числа ошибок на 0
                                   restore_tv_margin();       // окошко с числом ошибок на начало

                                   int resourceId2 = res.getIdentifier("win", "raw", package_name);
                                   //playFromResource(resourceId2);  //  звук победы
                                   b_check.setEnabled(true);
                                   b_check.setClickable(true);
                               }
                           }, 3000);

                       }
                        else {
                           new Handler().postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   //Draw2D.clear_canvas();
                                  // dd.invalidate();
                                   dd.change_letter_part();
                                   //error_progress.setProgress(0);  //  прогресс числа ошибок на 0
                                //   restore_tv_margin();       // окошко с числом ошибок на начало

                                   int resourceId2 = res.getIdentifier("win", "raw", package_name);
                                  // playFromResource(resourceId2);  //  звук победы
                                   b_check.setEnabled(true);
                                   b_check.setClickable(true);
                               }
                           }, 1000);
                             }// смена части буквы или самой буквы   (часть юуквы для китайского)
                        /*
                        error_progress.setProgress(0);  //  прогресс числа ошибок на 0
                        restore_tv_margin();       // окошко с числом ошибок на начало

                        int resourceId2 = res.getIdentifier("win", "raw", package_name);
                        playFromResource(resourceId2);  //  звук победы
                        */



                    }  else if (dd.i_error < dd.num_max_error) {      //  ошибок больше процентного предела (меньще 100)

                        int resourceId3 = res.getIdentifier("click", "raw", package_name);
                        playFromResource(resourceId3);
                        Toast.makeText(
                                context,
                                res.getString(R.string.try_again),
                                Toast.LENGTH_SHORT).show();

                        Draw2D.clear_canvas();
                        dd.invalidate();
                        error_progress.setProgress(0);
                        restore_tv_margin();
                        b_check.setEnabled(true);
                        b_check.setClickable(true);
                    }   else {                     //  ошибок больше процентного предела (больше 100)
                        int resourceId4 = res.getIdentifier("click", "raw", package_name);
                        playFromResource(resourceId4);
                        Toast.makeText(
                                context,
                                res.getString(R.string.failed),
                                Toast.LENGTH_SHORT).show();

                        Draw2D.clear_canvas();
                        dd.invalidate();
                        error_progress.setProgress(0);
                        restore_tv_margin();
                        b_check.setEnabled(true);
                        b_check.setClickable(true);
                    }
                }

        }});

        // проигрываем произношение буквы
        b_sound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
           play_letter();

            }
        });
        //  перходим на активити с выбором буквы
        b_letters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int resourceId = res.getIdentifier("click", "raw", package_name);
                playFromResource(resourceId);
                Intent i = new Intent(Go.this, LetterSelectActivity.class);
                startActivityForResult(i, 1);     //    ждём от него ответа
            }
        });





        // прогрессбар с числом ошибок
        error_progress = (ProgressBar) findViewById(R.id.error_progress);
        error_progress.setMax(Draw2D.num_max_error);

        // рожица
        iv_smile = (ImageView) findViewById(R.id.iv_smile);
        tv_error_count = (TextView) findViewById(R.id.tv_error_count);
        tv_error_count.setText(Html.fromHtml("<font color=red>&nbsp;0"  + "</font>" + "&nbsp;/30" ));



        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)tv_error_count.getLayoutParams();
        initial_right_margin = params.rightMargin;
        initial_bottom_margin = params.bottomMargin;
        initial_top_margin = params.topMargin;



       // ProgressBar pg = new ProgressBar(context, null,
      //          android.R.attr.progressBarStyleHorizontal);

        // тема для прогресс бара----------------------------------------------
        final float[] roundedCorners = new float[] { 10, 10, 10, 10,
                10, 10, 10, 10 };
        ShapeDrawable pgDrawable = new ShapeDrawable(
                new RoundRectShape(roundedCorners, null, null));
        String MyColor = "#FF0000";
        pgDrawable.getPaint().setColor(Color.parseColor(MyColor));
        pgDrawable.getPaint().setAlpha(150);
        // pgDrawable.getPaint()
        ClipDrawable progress = new ClipDrawable(pgDrawable,
                Gravity.LEFT, ClipDrawable.HORIZONTAL);
        // progress.setAlpha(10);
        error_progress.setProgressDrawable(progress);
        // pg.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.progress_horizontal));
        error_progress.setBackgroundDrawable(getResources().getDrawable(
                R.drawable.progressbar1));
        // тема для прогресс бара----------------------------------------------


        res = getResources();

        vozdaytel = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE) ;
        //LinearLayout  ll = (LinearLayout) findViewById(R.id.ll);
       // ll.*/
    }

    //  обрабатываем результат от активити с выбором буквы
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {

            if(resultCode == RESULT_OK){
                // большие и маленткие буквы в listview чередуются: A a B b C c D d E e ...
                String item = data.getStringExtra("item");
                int position = data.getIntExtra("position", 1);
                letter_current = position ;
                dd.change_letter(0);
                dd.invalidate();
                error_progress.setProgress(0);
                Draw2D.i_point = 0;
                Draw2D.i_error = 0;
                restore_tv_margin();
                iv_smile.setImageResource(R.drawable.normal);
                letter_part_current = 0;
                /*
                if (position % 2 == 0) {                    // кликнули по большой букве
                   letter_current = position / 2;
                    letter_capital = "b";
                    dd.change_letter(0);
                    dd.invalidate();
                    error_progress.setProgress(0);
                    Draw2D.i_point = 0;
                    Draw2D.i_error = 0;
                    restore_tv_margin();
                    iv_smile.setImageResource(R.drawable.normal);
                }
                else {                                     // кликнули по маленькой
                    letter_current = (position-1) / 2;
                    letter_capital = "s";
                    dd.change_letter(0);
                    dd.invalidate();
                    error_progress.setProgress(0);
                    Draw2D.i_point = 0;
                    Draw2D.i_error = 0;
                    restore_tv_margin();
                    iv_smile.setImageResource(R.drawable.normal);
                }
                */
            }

            if (resultCode == RESULT_CANCELED) {

                //Write your code on no result return

            }
        }//
    }

    public static int letter_count = 33; // число букв алфавита ; 33 - дефолтное для русского
    public static String locale_current;

    public static int letter_current = 1;     // номер текущей буквы в алфавите
    public static int letter_part_current = 0;     // часть текущей буквы
    public static  String letter_capital = "b";  //большая ли она: b или маленькая: s
    public static  String package_name;
    public static int letter_type = 0; // 0 - режим что все букы сразу,  1 - по частям выводится (китайский)

    public void global_init(){

         letter_current = 0;
        letter_capital = "b";

        // получение текущей локали
        locale_current = Locale.getDefault().getLanguage();
        if (locale_current.toLowerCase().contains("en"))
        {
            letter_count = 26;
            letter_type = 0;
        }
        else if (locale_current.toLowerCase().contains("ru") )
        {
            letter_count = 33;
            letter_type = 0;
        }
        else if (locale_current.toLowerCase().contains("zh"))
        {
            letter_count = 400;
            letter_type = 1;
        };
    }

    //  обработка рисования
    public static class  Draw2D extends View{


        static {
            w = Resources.getSystem().getDisplayMetrics().widthPixels;
            h = Resources.getSystem().getDisplayMetrics().heightPixels;
        }


        private static final int h;
        private static final int w;
        private Context _context;

        private float old_x, old_y;
        private static final float TOUCH_TOLERANCE = 4;

        static private Bitmap mBitmap_canvas;
        static private Bitmap bm, bm2;
        static float x_prev;
        static float y_prev;
        static float x_prev1 = 0F;
        static float y_prev1 = 0F;
        static private Canvas canvas;

        static int i_point = 0;
        static int i_error = 0;
        static int margin_top = -40;
        static int num_max_error = 30;
        static int num_min_achieved = 30;
        static float err_percent = 0.6F;
        static Paint paint1 = new Paint();
        static Paint paint2 = new Paint();
        static Paint mPaint = new Paint();
        Paint p;
        static boolean upb;
        static private MaskFilter mEmboss;
        static private MaskFilter mBlur;

        static String package_name;// = "playpad.education.orthography";

        static private Path path = new Path();

        //static private Canvas mCanvas;

        // private Path mPath_bar;

        static private Paint mBitmapPaint;
        public Draw2D(Context context) {

            super(context);
            //String g = Locale.getDefault().getDisplayLanguage();

            _context = context;
            initialization();
        }


        public Draw2D(Context context, AttributeSet attrs){

            super(context, attrs);
            _context = context;
           // String g = Locale.getDefault().getLanguage();
            initialization();
        }

        public Draw2D(Context context, AttributeSet attrs, int defStyle){
            super(context, attrs);
            _context = context;
            initialization();
        }

        @Override
        protected void onDraw(Canvas canvas){
            super.onDraw(canvas);




            canvas.drawBitmap(bm, 0, margin_top, p);


            canvas.drawPath(path, mPaint);
            canvas.drawBitmap(bm2, 0, margin_top, mBitmapPaint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {

            float x = event.getX();
            float y = event.getY();
            if ((x < w) && (y - margin_top < h)) {
                try

                {

                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            touch_start(x, y);
                            invalidate();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            touch_move(x, y);
                            invalidate();
                            break;
                        case MotionEvent.ACTION_UP:
                            touch_up(x, y);
                            invalidate();
                            break;
                    }

                    // --------------------------------

                    // ---------------------------------
                } catch (Exception e) {

                }
            }
            return true;
        }


        private void touch_start(float x, float y) {
                upb = true;
                if (y <= h ) {
                    path.moveTo(x, y);

                    old_x = x;
                    old_y = y;
                }


        }


        private void touch_move(float x, float y) {
            try {
                if ((y <= h ) && (upb == true)) {
                    float dx = Math.abs(x - old_x);
                    float dy = Math.abs(y - old_y);
                    if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                        path.quadTo(old_x, old_y, (x + old_x) / 2, (y + old_y) / 2);
                        old_x = x;
                        old_y = y;
                        y = y - margin_top;


                        int cl = bm.getPixel((int) x, (int) y);

                        int i1 = 1;

                        int cl0;

                        if (cl != 0) {
                            cl0 = Color.GREEN;
                            i_point++;

                        } else {
                            cl0 = Color.RED;
                            //propis0.vibrator.vibrate(30);

                            i_error++;
                            error_progress.setProgress(i_error);
                            if (i_error > num_max_error)
                                i_error = num_max_error;

                            move_textview();  //move tv_error_count
                            vozdaytel.vibrate(100);
                            tv_error_count.setText(Html.fromHtml("<font color=red>&nbsp;" + Integer.toString(i_error) + "</font>" + "&nbsp;/" + Integer.toString(num_max_error)));
                            if (i_error >= num_max_error )
                            {
                               // iv_smile.setBackgroundDrawable(res.getDrawable(R.drawable.unpleased));
                                Object a =   findViewById(R.id.iv_smile);
                                //iv_smile = (ImageView) findViewById(R.id.iv_smile);
                                iv_smile.setImageResource(R.drawable.unpleased);

                                achieved_critical_error_level = true;
                            }

                        }

                        if (cl0 == Color.RED) {
                            bm2.setPixel((int) x, (int) y, cl0);

                            for (int i = -i1; i <= i1; i++) {
                                for (int j = -i1; j <= i1; j++) {
                                    bm2.setPixel((int) x + i, (int) y
                                            + j, cl0);
                                    bm2.setPixel((int) x + i, (int) y
                                            + j, cl0);
                                    bm2.setPixel((int) x + i, (int) y
                                            + j, cl0);
                                    bm2.setPixel((int) x + i, (int) y
                                            + j, cl0);
                                }
                            }

                        }

                        double xy = Math.sqrt(((x_prev1 - x) * (x_prev1 - x))
                                + ((y_prev1 - y) * (y_prev1 - y)));
                        if (xy > 15) {

                        }
                        x_prev1 = x;
                        y_prev1 = y;
                        y = y + margin_top;
                    }
                }
            } catch (Exception e) {
             //   Log.e(TAG, "touch_move error", e);
            }
        }


        private void touch_up(float x, float y) {

            try {

                if (y <= h) {



                    path.lineTo(old_x, old_y);
                    // commit the path to our offscreen
                    canvas.drawPath(path, mPaint);
                    // kill this so we don't double draw
                    //path.reset();
                }

            } catch (Exception e) {
               // Log.e(TAG, "touch_up error", e);
            }


            if (achieved_critical_error_level){
                int resourceId = res.getIdentifier("lose", "raw", package_name);
                playFromResource(resourceId, _context);
                Toast.makeText(
                        _context,
                        res.getString(R.string.failed),
                        Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        clear_canvas();
                        invalidate();
                        restore_tv_margin();
                        achieved_critical_error_level = false;
                    }
                }, 1000);


            }
        }

        private boolean achieved_critical_error_level = false;


        Resources res;




        public void change_letter_part(){
            i_point = 0;
            //i_error = 0;       //path.rewind();
            String res_name = "";
            Go.letter_part_current++;
            res_name = "q_"+ Integer.toString(Go.letter_current) + "_" +
                    Integer.toString(Go.letter_part_current) ;

            int drawableResourceId = res.getIdentifier(res_name, "drawable", package_name);

            Draw2D.clear_canvas2();
            invalidate();
            if (drawableResourceId == 0  ){  // не нашли
                Go.letter_part_current = 0;
                iv_smile.setImageResource(R.drawable.happy);


                String res_name2 =   "q_"+ Integer.toString(Go.letter_current) + "_f";
                int drawableResourceId2 = res.getIdentifier(res_name2, "drawable", package_name);
                if (drawableResourceId2 !=0) {
                    bm=BitmapFactory.decodeResource(res, drawableResourceId2);

                    bm = Bitmap.createScaledBitmap(bm, w, h,
                            true);
                    invalidate();
                    play_letter();
                }

                //b_sound.performClick();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        change_letter(1);
                        iv_smile.setImageResource(R.drawable.normal);
                        error_progress.setProgress(0) ;
                        restore_tv_margin();
                       invalidate();
                    }
                }, 5000);
                          // нету частей для этой буквы -- всю собрали -- переходим на следующую
                return;
            }
            bm=BitmapFactory.decodeResource(res, drawableResourceId);

            bm = Bitmap.createScaledBitmap(bm, w, h,
                    true);
            invalidate();
        }


        public  void  play_letter(){
            int a = Go.letter_current;
            if (Go.letter_current == 0)
                a = 1;

            if (locale_current.toLowerCase().contains("ru") ||  locale_current.toLowerCase().contains("en"))     {
                if (a % 2 == 0)
                    a = a / 2;
                else a = a / 2 + 1;
            }


            String res_name = "char_"+ Integer.toString(a);

            int resourceId2 = res.getIdentifier(res_name, "raw", package_name);

            playFromResource(resourceId2, _context);
        }

        public static void clear_canvas(){
            i_error = 0;
            i_point = 0;
            path.reset();

            bm2 = Bitmap.createBitmap(w, h,
                    Bitmap.Config.ARGB_4444);

            error_progress.setProgress(0);
            iv_smile.setImageResource(R.drawable.normal);
        }


        public static void clear_canvas2(){
           // i_error = 0;
            i_point = 0;
            path.reset();

            bm2 = Bitmap.createBitmap(w, h,
                    Bitmap.Config.ARGB_4444);

          //  error_progress.setProgress(0);
            iv_smile.setImageResource(R.drawable.normal);
        }

        public void change_letter(int param){
            i_point = 0;
            i_error = 0;


            if (locale_current.toLowerCase().contains("ru") ||  locale_current.toLowerCase().contains("en")) {
               // play_letter();
            }   else {}


            path.rewind();
            bm2 = Bitmap.createBitmap(w, h,
                    Bitmap.Config.ARGB_4444);

            String res_name = "";
            if( Go.letter_current == 52 && locale_current.toLowerCase().contains("en") )
                Go.letter_current = 1;
            else if (Go.letter_current == 63  && locale_current.toLowerCase().contains("ru"))
                Go.letter_current = 1;
            else if  (Go.letter_current == 48  && locale_current.toLowerCase().contains("zh"))
                Go.letter_current = 1;
            else
                Go.letter_current++;
            //не первый запуск активити и сейчас маленькая:
            /*
            if (Go.letter_current != 0  && Go.letter_capital.equals("s") )  {
                if (param != 0)   {  // не с ListView выбора букв пришли
                Go.letter_current++;          // меняем на следующую по номеру и большую
                Go.letter_capital = "b";
                } else      Go.letter_current++ ;
            }
            // не первый запуск активити и сейчас на большой букве, то меняем на маленькую
            else if (Go.letter_current != 0 && Go.letter_capital.equals("b")){
                if (param != 0) Go.letter_capital = "s" ; else Go.letter_current++ ;
            }
             //первый запуск активити:
             if (Go.letter_current == 0 )
                 Go.letter_current++;

             */
             res_name =  "q_"+ Integer.toString(Go.letter_current);


            int drawableResourceId = res.getIdentifier(res_name, "drawable", package_name);

            if (drawableResourceId == 0){  // не нашли
                return;
            }
            bm=BitmapFactory.decodeResource(res, drawableResourceId);

            bm = Bitmap.createScaledBitmap(bm, w, h,
                    true);
            invalidate();
        }

        private void initialization(){

            package_name = Go.package_name;

            if (locale_current.toLowerCase().contains("ru") ||  locale_current.toLowerCase().contains("en"))
                num_min_achieved = 25;
            else num_min_achieved = 4;

            p =new Paint();
            res = getResources();
            /*
            String res_name =  Go.letter_capital + "_"+ Go.letter_current;

            int drawableResourceId = res.getIdentifier(res_name, "drawable", package_name);
            bm=BitmapFactory.decodeResource(res, drawableResourceId);

            bm = Bitmap.createScaledBitmap(bm, w, h,
                    true);

                    */
            change_letter(1);

            p.setColor(Color.RED);


            bm2 = Bitmap.createBitmap(w, h,
                    Bitmap.Config.ARGB_4444);

            paint1.setColor(Color.RED);
            paint1.setStyle(Paint.Style.FILL);
            paint1.setTextSize(15);
            paint1.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

            // paint2 = new Paint();
            paint2.setColor(Color.WHITE);
            paint2.setStyle(Paint.Style.FILL);
            paint2.setTextSize(15);
            paint2.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

            // mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setDither(true);
            mPaint.setColor(0xFF192868); // цвет чернил
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeJoin(Paint.Join.ROUND);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(12);

            // mEmboss = new EmbossMaskFilter(new float[] { 1, 1, 1 }, 0.4f,
            // 6, 3.5f);
            mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);
            if (mPaint.getMaskFilter() != mEmboss) {
                mPaint.setMaskFilter(mEmboss);
            } else {
                mPaint.setMaskFilter(null);
            }

         //   mCanvas = new Canvas(bm2);
            path = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);




        }


        public static MediaPlayer _mediaPlayer;

        public  void playFromResource(int resId, Context c)
        {
            if (_mediaPlayer != null)
            {
                _mediaPlayer.reset();
            }
            _mediaPlayer = MediaPlayer.create(c, resId);
            _mediaPlayer.start();
        }

        public static int k = 0       ;
    }

}
